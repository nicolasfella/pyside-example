import QtQuick 2.2
import QtQuick.Controls 2.5
import org.kde.test 1.0
import QtQuick.Layouts 1.1

ApplicationWindow {
    width: 400
    height: 400

    visible: true

    title: "Python + QML example"

    ExampleClass {
        id: example
    }

    Button {
        text: example.testprop
        anchors.centerIn: parent
        onClicked: example.testprop = "👨‍💻 Happy hacking! 👩🏽‍💻"
    }
}
